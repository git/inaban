// SPDX-FileCopyrightText: 2019 tinyWL Authors
// SPDX-License-Identifier: CC0-1.0
//
// SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef INABAN_H
#define INABAN_H
#include <stdbool.h>
#include <wayland-server.h>
#include <xkbcommon/xkbcommon.h>

// stable wlroots interfaces
#include <wlr/types/wlr_matrix.h>
#include <wlr/util/log.h>

// unstable wlroots interfaces
#define WLR_USE_UNSTABLE
#include <wlr/backend.h>
#include <wlr/render/allocator.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_data_control_v1.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_pointer.h>
#include <wlr/types/wlr_primary_selection.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_server_decoration.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_decoration_v1.h>
#include <wlr/types/wlr_xdg_shell.h>
#undef WLR_USE_UNSTABLE

enum inaban_input_mode
{
	INABAN_INPUT_NORMAL,
	INABAN_INPUT_MODKEY,
};

enum inaban_cursor_mode
{
	INABAN_CURSOR_NORMAL,
	INABAN_CURSOR_MOVE,
	INABAN_CURSOR_RESIZE,
};

struct inaban_server
{
	struct wl_display *wl_display;
	struct wlr_backend *backend;
	struct wlr_renderer *renderer;
	struct wlr_allocator *allocator;

	struct wlr_xdg_shell *xdg_shell;
	struct wl_listener new_xdg_surface;
	struct wl_list views;

	struct wl_listener xdg_toplevel_decoration;

	struct wlr_cursor *cursor;
	struct wlr_xcursor_manager *cursor_mgr;
	struct wl_listener cursor_motion;
	struct wl_listener cursor_motion_absolute;
	struct wl_listener cursor_button;
	struct wl_listener cursor_axis;
	struct wl_listener cursor_frame;

	struct wlr_seat *seat;
	struct wl_listener new_input;
	struct wl_listener request_cursor;
	struct wl_list keyboards;

	struct wlr_output_layout *output_layout;
	struct wl_list outputs;
	struct wl_listener new_output;

	enum inaban_input_mode input_mode;
	enum inaban_cursor_mode cursor_mode;

	struct inaban_view *grabbed_view;

	/* cursor position */
	double grab_x, grab_y;

	/* window size */
	int grab_width, grab_height;

	/* clipboard */
	struct wl_listener request_set_primary_selection;
	struct wl_listener request_set_selection;

	bool locked;
};

struct inaban_output
{
	struct wl_list link;
	struct inaban_server *server;
	struct wlr_output *wlr_output;
	struct wl_listener frame;
	struct wl_listener mode;
};

struct inaban_view
{
	struct wl_list link;
	struct inaban_server *server;
	struct wlr_xdg_surface *xdg_surface;
	struct wl_listener map;
	struct wl_listener unmap;
	struct wl_listener destroy;
	struct wl_listener request_move;
	struct wl_listener request_resize;
	bool mapped;
	int x, y;
};

struct inaban_keyboard
{
	struct wl_list link;
	struct inaban_server *server;
	struct wlr_input_device *device;

	struct wl_listener modifiers;
	struct wl_listener key;
};

/* Used to move all of the data necessary to render a surface from the top-level
 * frame handler to the per-surface render function. */
struct render_data
{
	struct wlr_output *output;
	struct wlr_renderer *renderer;
	struct inaban_view *view;
	struct timespec *when;
};

typedef union
{
	int i;
	unsigned int ui;
	float f;
	const void *v;
} Arg;

typedef struct
{
	uint32_t mod;
	xkb_keysym_t keysym;
	void (*func)(const Arg *);
	const Arg arg;
} Shortcut;

void spawn(const Arg *arg);
void focus_view(struct inaban_view *view, struct wlr_surface *surface);
void quit(const Arg *arg);
void lock(const Arg *arg);
#endif /* INABAN_H */
