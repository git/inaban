<!--
SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
SPDX-License-Identifier: BSD-3-Clause
-->

# Accessibility
Until version 1.0 this serves as a roadmap.

## Generic
- On first startup, it should try to launch orca and quickly ask if it will be needed by the user
- Interface should be usable with only a 2-button pointer or only a keyboard

## Lockscreen
- Username, machine name
- Keyboard layout and modifier status
- "Access Granted" / "Access Denied" messages
- Support for braille(brltty) and speech output
