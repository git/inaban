// SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
// SPDX-License-Identifier: BSD-3-Clause

#include "inaban.h"

#include <stdlib.h> // free()

struct inaban_xdg_decoration
{
	struct wlr_xdg_toplevel_decoration_v1 *wlr_decoration;
	struct inaban_server *server;
	struct wl_listener destroy;
	struct wl_listener request_mode;
};

static void
xdg_decoration_handle_destroy(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_xdg_decoration *xdg_decoration = wl_container_of(listener, xdg_decoration, destroy);

	wl_list_remove(&xdg_decoration->destroy.link);
	wl_list_remove(&xdg_decoration->request_mode.link);
	free(xdg_decoration);
}

static void
xdg_decoration_handle_request_mode(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_xdg_decoration *xdg_decoration =
	    wl_container_of(listener, xdg_decoration, request_mode);

	wlr_xdg_toplevel_decoration_v1_set_mode(xdg_decoration->wlr_decoration,
	                                        WLR_XDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
}

void
handle_xdg_toplevel_decoration(struct wl_listener *listener, void *data)
{
	struct inaban_server *server = wl_container_of(listener, server, xdg_toplevel_decoration);
	struct wlr_xdg_toplevel_decoration_v1 *wlr_decoration = data;

	struct inaban_xdg_decoration *xdg_decoration = calloc(1, sizeof(struct inaban_xdg_decoration));
	if(!xdg_decoration)
	{
		return;
	}

	xdg_decoration->wlr_decoration = wlr_decoration;
	xdg_decoration->server         = server;

	xdg_decoration->destroy.notify = xdg_decoration_handle_destroy;
	wl_signal_add(&wlr_decoration->events.destroy, &xdg_decoration->destroy);
	xdg_decoration->request_mode.notify = xdg_decoration_handle_request_mode;
	wl_signal_add(&wlr_decoration->events.request_mode, &xdg_decoration->request_mode);

	xdg_decoration_handle_request_mode(&xdg_decoration->request_mode, wlr_decoration);
}
