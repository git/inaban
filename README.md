<!--
SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
SPDX-License-Identifier: BSD-3-Clause
-->

## inaban
Inaban: Nickname of “稲葉姫子 (Inaba Himeko)”, a character in Kokoro Connect. Picked her for her personality regarding reality and trust.

# Dependencies
- wlroots 0.13.0+
- Wayland: wayland-server, wayland-scanner, wayland-protocols
- (lib)xkbcommon
- a [`checkpassword`](https://cr.yp.to/checkpwd.html) implementation

# Installation
The usual `make ; make install` works. Running inaban as root (setuid included) is unsupported, you need to use something like seatd or {e,systemd-}logind.

# Inspirations
## XMonad
- Most of the shortcuts

## dwm
- configuration and related code

## cage, sway, rootston
- wlroots related code

## Design and Goals/Non-Goals/…
Refer to:
- <./security.md>
- <./accessibility.md>

# Goals
- No client side decorations, might have borders on all the boxes (rendering and hitbox, should they ever manage to differ)
- Programs wanting to have more than simply asking for a regular surface should need special permissions
	- listing of the surfaces should be something similar to a capability
	- special position/size should be completely managed by the compositor
	- resizes other than done by the compositor are denied
- If there is tiling should be powerful (like XMonad) yet simple (unlike i3)

## Non-Goals
- XWayland as anything but a separated Wayland client (would recommend cage for this)
