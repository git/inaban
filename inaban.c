// SPDX-FileCopyrightText: 2019 tinyWL Authors
// SPDX-License-Identifier: CC0-1.0
//
// SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
// SPDX-License-Identifier: BSD-3-Clause

#include "inaban.h"

#include "config.h"

#include <assert.h>
#include <getopt.h>
#include <signal.h> /* signal(), SIGTERM */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> /* execvp() */
#include <unistd.h>

#define LENGTH(X) (sizeof X / sizeof X[0])
#define MAX_STARTUP_CMD 256

struct inaban_server server = {0};

// xdg-decoration.c
extern void handle_xdg_toplevel_decoration(struct wl_listener *listener, void *data);

/* event raised when a modifier key, such as shift or alt, is pressed. */
static void
keyboard_handle_modifiers(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_keyboard *keyboard = wl_container_of(listener, keyboard, modifiers);
	struct wlr_seat *seat            = keyboard->server->seat;

	if(server.locked == true)
	{
		// needs to be written
	}
	else
	{
		wlr_seat_set_keyboard(seat, keyboard->device);
		wlr_seat_keyboard_notify_modifiers(seat, &keyboard->device->keyboard->modifiers);
	}
}

/* event raised when a key is pressed or released. */
static void
keyboard_handle_key(struct wl_listener *listener, void *data)
{
	struct inaban_keyboard *keyboard     = wl_container_of(listener, keyboard, key);
	struct inaban_server *server         = keyboard->server;
	struct wlr_event_keyboard_key *event = data;
	struct wlr_seat *seat                = server->seat;

	/* Translate libinput keycode -> xkbcommon */
	uint32_t keycode    = event->keycode + 8;
	xkb_keysym_t keysym = xkb_state_key_get_one_sym(keyboard->device->keyboard->xkb_state, keycode);

	bool handled       = false;
	uint32_t modifiers = wlr_keyboard_get_modifiers(keyboard->device->keyboard);

	if(server->locked == true)
	{
		if((event->state == WL_KEYBOARD_KEY_STATE_PRESSED) &&
		   (keysym >= XKB_KEY_XF86Switch_VT_1 && keysym <= XKB_KEY_XF86Switch_VT_12))
		{
			struct wlr_session *session = wlr_backend_get_session(server->backend);
			if(session)
			{
				unsigned vt = keysym - XKB_KEY_XF86Switch_VT_1 + 1;
				wlr_session_change_vt(session, vt);
			}
		}

		return;
	}

	switch(event->state)
	{
	case WL_KEYBOARD_KEY_STATE_PRESSED:
		wlr_log(WLR_DEBUG,
		        "key_pressed: {modifiers: %x, keycode: %x, keysym: %x}",
		        modifiers,
		        keycode,
		        keysym);

		if(keysym >= XKB_KEY_XF86Switch_VT_1 && keysym <= XKB_KEY_XF86Switch_VT_12)
		{
			struct wlr_session *session = wlr_backend_get_session(server->backend);
			if(session)
			{
				unsigned vt = keysym - XKB_KEY_XF86Switch_VT_1 + 1;
				wlr_session_change_vt(session, vt);
			}
		}

		for(size_t i = 0; i < LENGTH(shortcuts); i++)
			if((keysym == shortcuts[i].keysym) && (modifiers == shortcuts[i].mod) && shortcuts[i].func)
			{
				shortcuts[i].func(&(shortcuts[i].arg));
				handled = true;
			}

		if(!handled && keysym == ModKey)
		{
			wlr_log(WLR_DEBUG, "InputMode: ModKey");
			server->input_mode = INABAN_INPUT_MODKEY;
			handled            = true;
		}
		break;
	case WL_KEYBOARD_KEY_STATE_RELEASED:
		if(keysym == ModKey && server->input_mode == INABAN_INPUT_MODKEY)
		{
			wlr_log(WLR_DEBUG, "InputMode: Normal");
			server->input_mode = INABAN_INPUT_NORMAL;
			handled            = true;
		}
		break;
	}

	/* Otherwise, we pass it along to the client. */
	if(!handled)
	{
		wlr_seat_set_keyboard(seat, keyboard->device);
		wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode, event->state);
	}
}

static void
server_new_keyboard(struct inaban_server *server, struct wlr_input_device *device)
{
	struct inaban_keyboard *keyboard = calloc(1, sizeof(struct inaban_keyboard));
	keyboard->server                 = server;
	keyboard->device                 = device;

	/* We need to prepare an XKB keymap and assign it to the keyboard. This
	 * assumes the defaults (e.g. layout = "us"). */
	struct xkb_rule_names rules = {0};
	struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	struct xkb_keymap *keymap = xkb_map_new_from_names(context, &rules, XKB_KEYMAP_COMPILE_NO_FLAGS);

	wlr_keyboard_set_keymap(device->keyboard, keymap);
	xkb_keymap_unref(keymap);
	xkb_context_unref(context);
	wlr_keyboard_set_repeat_info(device->keyboard, 25, 600);

	/* Here we set up listeners for keyboard events. */
	keyboard->modifiers.notify = keyboard_handle_modifiers;
	wl_signal_add(&device->keyboard->events.modifiers, &keyboard->modifiers);
	keyboard->key.notify = keyboard_handle_key;
	wl_signal_add(&device->keyboard->events.key, &keyboard->key);

	wlr_seat_set_keyboard(server->seat, device);

	/* And add the keyboard to our list of keyboards */
	wl_list_insert(&server->keyboards, &keyboard->link);
}

static void
server_new_pointer(struct inaban_server *server, struct wlr_input_device *device)
{
	/* We don't do anything special with pointers. All of our pointer handling
	 * is proxied through wlr_cursor. On another compositor, you might take this
	 * opportunity to do libinput configuration on the device to set
	 * acceleration, etc. */
	wlr_cursor_attach_input_device(server->cursor, device);
}

/* event raised by the backend when a new input device becomes available */
static void
server_new_input(struct wl_listener *listener, void *data)
{
	struct inaban_server *server    = wl_container_of(listener, server, new_input);
	struct wlr_input_device *device = data;
	switch(device->type)
	{
	case WLR_INPUT_DEVICE_KEYBOARD: server_new_keyboard(server, device); break;
	case WLR_INPUT_DEVICE_POINTER: server_new_pointer(server, device); break;
	default: break;
	}
	/* We need to let the wlr_seat know what our capabilities are, which is
	 * communiciated to the client. In TinyWL we always have a cursor, even if
	 * there are no pointer devices, so we always include that capability. */
	uint32_t caps = WL_SEAT_CAPABILITY_POINTER;
	if(!wl_list_empty(&server->keyboards)) caps |= WL_SEAT_CAPABILITY_KEYBOARD;

	wlr_seat_set_capabilities(server->seat, caps);
}

/* event raised by the seat when a client provides a cursor image */
static void
seat_request_cursor(struct wl_listener *listener, void *data)
{
	struct inaban_server *server = wl_container_of(listener, server, request_cursor);
	struct wlr_seat_pointer_request_set_cursor_event *event = data;
	struct wlr_seat_client *focused_client = server->seat->pointer_state.focused_client;

	if(focused_client == event->seat_client)
		wlr_cursor_set_surface(server->cursor, event->surface, event->hotspot_x, event->hotspot_y);
}

static bool
view_at(struct inaban_view *view,
        double lx,
        double ly,
        struct wlr_surface **surface,
        double *sx,
        double *sy)
{
	/*
	 * XDG toplevels may have nested surfaces, such as popup windows for context
	 * menus or tooltips. This function tests if any of those are underneath the
	 * coordinates lx and ly (in output Layout Coordinates). If so, it sets the
	 * surface pointer to that wlr_surface and the sx and sy coordinates to the
	 * coordinates relative to that surface's top-left corner.
	 */
	double view_sx = lx - view->x;
	double view_sy = ly - view->y;

	double _sx, _sy;
	struct wlr_surface *_surface = NULL;
	_surface = wlr_xdg_surface_surface_at(view->xdg_surface, view_sx, view_sy, &_sx, &_sy);

	if(_surface != NULL)
	{
		*sx      = _sx;
		*sy      = _sy;
		*surface = _surface;
		return true;
	}

	return false;
}

/* iterates over all of our surfaces and attempts to find one under the cursor,
 * relies on server->views being ordered from top-to-bottom */
static struct inaban_view *
desktop_view_at(struct inaban_server *server,
                double lx,
                double ly,
                struct wlr_surface **surface,
                double *sx,
                double *sy)
{
	struct inaban_view *view;
	wl_list_for_each(view, &server->views, link)
	{
		if(view_at(view, lx, ly, surface, sx, sy)) return view;
	}
	return NULL;
}

static void
process_cursor_motion(struct inaban_server *server, uint32_t time)
{
	if(server->cursor_mode == INABAN_CURSOR_MOVE)
	{
		struct inaban_view *view = server->grabbed_view;

		view->x = (int)server->cursor->x - (int)server->grab_x;
		view->y = (int)server->cursor->y - (int)server->grab_y;
	}
	else if(server->cursor_mode == INABAN_CURSOR_RESIZE)
	{
		struct inaban_view *view = server->grabbed_view;

		uint32_t width  = (uint32_t)server->cursor->x - (uint32_t)server->grab_x;
		uint32_t height = (uint32_t)server->cursor->y - (uint32_t)server->grab_y;

		wlr_xdg_toplevel_set_size(view->xdg_surface, width, height);
	}
	else
	{
		double sx, sy;
		struct wlr_seat *seat       = server->seat;
		struct wlr_surface *surface = NULL;
		struct inaban_view *view =
		    desktop_view_at(server, server->cursor->x, server->cursor->y, &surface, &sx, &sy);

		/* If there's no view under the cursor, set the cursor image to a
	 * default. This is what makes the cursor image appear when you move it
	 * around the screen, not over any views. */
		if(!view) wlr_xcursor_manager_set_cursor_image(server->cursor_mgr, "left_ptr", server->cursor);

		if(surface)
		{
			bool focus_changed = seat->pointer_state.focused_surface != surface;
			/*
		 * "Enter" the surface if necessary. This lets the client know that the
		 * cursor has entered one of its surfaces.
		 *
		 * Note that this gives the surface "pointer focus", which is distinct
		 * from keyboard focus. You get pointer focus by moving the pointer over
		 * a window.
		 */
			wlr_seat_pointer_notify_enter(seat, surface, sx, sy);
			/* The enter event contains coordinates, so we only need to notify
		 * on motion if the focus did not change. */
			if(!focus_changed) wlr_seat_pointer_notify_motion(seat, time, sx, sy);
		}
		else
		{
			/* Clear pointer focus so future button events and such are not sent to
		 * the last client to have the cursor over it. */
			wlr_seat_pointer_clear_focus(seat);
		}
	}
}

static void
server_cursor_motion(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}
	/* This event is forwarded by the cursor when a pointer emits a _relative_
	 * pointer motion event (i.e. a delta) */
	struct inaban_server *server           = wl_container_of(listener, server, cursor_motion);
	struct wlr_event_pointer_motion *event = data;
	/* The cursor doesn't move unless we tell it to. The cursor automatically
	 * handles constraining the motion to the output layout, as well as any
	 * special configuration applied for the specific input device which
	 * generated the event. You can pass NULL for the device if you want to move
	 * the cursor around without any input. */
	wlr_cursor_move(server->cursor, event->device, event->delta_x, event->delta_y);
	process_cursor_motion(server, event->time_msec);
}

static void
server_cursor_motion_absolute(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}
	/* This event is forwarded by the cursor when a pointer emits an _absolute_
	 * motion event, from 0..1 on each axis. This happens, for example, when
	 * wlroots is running under a Wayland window rather than KMS+DRM, and you
	 * move the mouse over the window. You could enter the window from any edge,
	 * so we have to warp the mouse there. There is also some hardware which
	 * emits these events. */
	struct inaban_server *server = wl_container_of(listener, server, cursor_motion_absolute);
	struct wlr_event_pointer_motion_absolute *event = data;
	wlr_cursor_warp_absolute(server->cursor, event->device, event->x, event->y);
	process_cursor_motion(server, event->time_msec);
}

/* event forwarded by the cursor when a pointer emits a button event */
static void
server_cursor_button(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}

	struct inaban_server *server           = wl_container_of(listener, server, cursor_button);
	struct wlr_event_pointer_button *event = data;
	double sx, sy;
	struct wlr_surface *surface = NULL;
	struct inaban_view *view =
	    desktop_view_at(server, server->cursor->x, server->cursor->y, &surface, &sx, &sy);

	if((server->input_mode == INABAN_INPUT_MODKEY) && (server->cursor_mode == INABAN_CURSOR_NORMAL) &&
	   (event->state == WLR_BUTTON_PRESSED) && (view != NULL))
	{
		struct wlr_box geo_box;
		wlr_xdg_surface_get_geometry(view->xdg_surface, &geo_box);

		switch(event->button)
		{
		case 272: /* for some reason this is button 1 */
			server->cursor_mode  = INABAN_CURSOR_MOVE;
			server->grabbed_view = view;
			server->grab_x       = server->cursor->x - view->x;
			server->grab_y       = server->cursor->y - view->y;
			wlr_log(WLR_DEBUG, "server->cursor_mode = move");
			break;
		case 273: /* for some reason this is button 2 */
			server->cursor_mode  = INABAN_CURSOR_RESIZE;
			server->grabbed_view = view;

			server->grab_x      = view->x;
			server->grab_y      = view->y;
			server->grab_width  = geo_box.width;
			server->grab_height = geo_box.height;
			wlr_log(WLR_DEBUG, "server->cursor_mode = resize");
			break;
		}
	}
	else if((server->cursor_mode != INABAN_CURSOR_NORMAL) && (event->state == WLR_BUTTON_RELEASED))
	{
		server->cursor_mode = INABAN_CURSOR_NORMAL;
	}
	else
	{
		/* Notify the client with pointer focus that a button press has occurred */
		wlr_seat_pointer_notify_button(server->seat, event->time_msec, event->button, event->state);
		/* Focus that client if the button was _pressed_ */
		focus_view(view, surface);
	}
}

static void
server_cursor_axis(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}

	/* This event is forwarded by the cursor when a pointer emits an axis event,
	 * for example when you move the scroll wheel. */
	struct inaban_server *server         = wl_container_of(listener, server, cursor_axis);
	struct wlr_event_pointer_axis *event = data;
	/* Notify the client with pointer focus of the axis event. */
	wlr_seat_pointer_notify_axis(server->seat,
	                             event->time_msec,
	                             event->orientation,
	                             event->delta,
	                             event->delta_discrete,
	                             event->source);
}

static void
server_cursor_frame(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}
	(void)data;
	/* This event is forwarded by the cursor when a pointer emits an frame
	 * event. Frame events are sent after regular pointer events to group
	 * multiple events together. For instance, two axis events may happen at the
	 * same time, in which case a frame event won't be sent in between. */
	struct inaban_server *server = wl_container_of(listener, server, cursor_frame);
	/* Notify the client with pointer focus of the frame event. */
	wlr_seat_pointer_notify_frame(server->seat);
}

/* called for every surface that needs to be rendered */
static void
render_surface(struct wlr_surface *surface, int sx, int sy, void *data)
{
	struct wlr_box border_box;

	struct render_data *rdata = data;
	struct inaban_view *view  = rdata->view;
	struct wlr_output *output = rdata->output;

	/* We first obtain a wlr_texture, which is a GPU resource. wlroots
	 * automatically handles negotiating these with the client. The underlying
	 * resource could be an opaque handle passed from the client, or the client
	 * could have sent a pixel buffer which we copied to the GPU, or a few other
	 * means. You don't have to worry about this, wlroots takes care of it. */
	struct wlr_texture *texture = wlr_surface_get_texture(surface);
	if(texture == NULL) return;

	/* The view has a position in layout coordinates. If you have two displays,
	 * one next to the other, both 1080p, a view on the rightmost display might
	 * have layout coordinates of 2000,100. We need to translate that to
	 * output-local coordinates, or (2000 - 1920). */
	double ox = 0, oy = 0;
	wlr_output_layout_output_coords(view->server->output_layout, output, &ox, &oy);
	ox += view->x + sx, oy += view->y + sy;

	/* We also have to apply the scale factor for HiDPI outputs. This is only
	 * part of the puzzle, TinyWL does not fully support HiDPI. */
	struct wlr_box box = {
	    .x      = (int)(ox * output->scale),
	    .y      = (int)(oy * output->scale),
	    .width  = (int)(surface->current.width * output->scale),
	    .height = (int)(surface->current.height * output->scale),
	};

	/*
	 * Those familiar with OpenGL are also familiar with the role of matricies
	 * in graphics programming. We need to prepare a matrix to render the view
	 * with. wlr_matrix_project_box is a helper which takes a box with a desired
	 * x, y coordinates, width and height, and an output geometry, then
	 * prepares an orthographic projection and multiplies the necessary
	 * transforms to produce a model-view-projection matrix.
	 *
	 * Naturally you can do this any way you like, for example to make a 3D
	 * compositor.
	 */
	float matrix[9];
	enum wl_output_transform transform = wlr_output_transform_invert(surface->current.transform);
	wlr_matrix_project_box(matrix, &box, transform, 0, output->transform_matrix);

	/* top border */
	border_box.x      = box.x - BORDER_SIZE;
	border_box.y      = box.y - BORDER_SIZE;
	border_box.width  = box.width + BORDER_SIZE * 2;
	border_box.height = BORDER_SIZE;
	wlr_render_rect(rdata->renderer, &border_box, border_color, output->transform_matrix);
	/* bottom border */
	border_box.y = box.y + box.height;
	wlr_render_rect(rdata->renderer, &border_box, border_color, output->transform_matrix);
	/* left border */
	border_box.x      = box.x - BORDER_SIZE;
	border_box.y      = box.y - BORDER_SIZE;
	border_box.width  = BORDER_SIZE;
	border_box.height = box.height + BORDER_SIZE * 2;
	wlr_render_rect(rdata->renderer, &border_box, border_color, output->transform_matrix);
	/* right border */
	border_box.x = box.x + box.width;
	wlr_render_rect(rdata->renderer, &border_box, border_color, output->transform_matrix);

	/* This takes our matrix, the texture, and an alpha, and performs the actual
	 * rendering on the GPU. */
	wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1);

	/* This lets the client know that we've displayed that frame and it can
	 * prepare another one now if it likes. */
	wlr_surface_send_frame_done(surface, rdata->when);
}

/* called every time an output is ready to display a frame, 
 * generally at the output's refresh rate (e.g. 60Hz). */
static void
output_frame(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_output *output  = wl_container_of(listener, output, frame);
	struct wlr_renderer *renderer = output->server->renderer;

	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	/* wlr_output_attach_render makes the OpenGL context current. */
	if(!wlr_output_attach_render(output->wlr_output, NULL))
	{
		return;
	}
	/* The "effective" resolution can change if you rotate your outputs. */
	int width, height;
	wlr_output_effective_resolution(output->wlr_output, &width, &height);
	/* Begin the renderer (calls glViewport and some other GL sanity checks) */
	wlr_renderer_begin(renderer, (uint32_t)width, (uint32_t)height);

	if(output->server->locked == true)
	{
		// cursor isn't draw because the cursor motion is ignored
		wlr_renderer_clear(renderer, locked_color);
		wlr_renderer_end(renderer);
		wlr_output_commit(output->wlr_output);
		return;
	}

	wlr_renderer_clear(renderer, background_color);
	/* Each subsequent window we render is rendered on top of the last. Because
	 * our view list is ordered front-to-back, we iterate over it backwards. */
	struct inaban_view *view;
	wl_list_for_each_reverse(view, &output->server->views, link)
	{
		if(!view->mapped) continue; /* An unmapped view should not be rendered. */
		struct render_data rdata = {
		    .output   = output->wlr_output,
		    .view     = view,
		    .renderer = renderer,
		    .when     = &now,
		};
		/* This calls our render_surface function for each surface among the
		 * xdg_surface's toplevel and popups. */
		wlr_xdg_surface_for_each_surface(view->xdg_surface, render_surface, &rdata);
	}

	/* Hardware cursors are rendered by the GPU on a separate plane, and can be
	 * moved around without re-rendering what's beneath them - which is more
	 * efficient. However, not all hardware supports hardware cursors. For this
	 * reason, wlroots provides a software fallback, which we ask it to render
	 * here. wlr_cursor handles configuring hardware vs software cursors for you,
	 * and this function is a no-op when hardware cursors are in use. */
	wlr_output_render_software_cursors(output->wlr_output, NULL);

	/* Conclude rendering and swap the buffers, showing the final frame
	 * on-screen. */
	wlr_renderer_end(renderer);
	wlr_output_commit(output->wlr_output);
}

static void
handle_output_mode(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_output *output  = wl_container_of(listener, output, mode);
	struct wlr_output *wlr_output = output->wlr_output;

	struct wlr_output_mode *mode = wlr_output_preferred_mode(wlr_output);
	if(mode)
	{
		wlr_output_set_mode(wlr_output, mode);
	}

	wlr_output_enable(wlr_output, true);
	wlr_output_commit(wlr_output);
}

/* raised by the backend when a new output (aka display/monitor) becomes available */
static void
server_new_output(struct wl_listener *listener, void *data)
{
	struct inaban_server *server  = wl_container_of(listener, server, new_output);
	struct wlr_output *wlr_output = data;

	/* Allocates and configures our state for this output */
	struct inaban_output *output = calloc(1, sizeof(struct inaban_output));
	if(output == NULL)
	{
		wlr_log_errno(WLR_ERROR, "Unable to allocate inaban output");
		return;
	}

	struct wlr_output_mode *mode = wlr_output_preferred_mode(wlr_output);
	if(mode)
	{
		wlr_output_set_mode(wlr_output, mode);
	}

	wlr_output_init_render(wlr_output, server->allocator, server->renderer);

	output->wlr_output = wlr_output;
	output->server     = server;
	/* Sets up a listener for the frame notify event. */
	output->frame.notify = output_frame;
	wl_signal_add(&wlr_output->events.frame, &output->frame);
	wl_list_insert(&server->outputs, &output->link);

	output->mode.notify = handle_output_mode;
	wl_signal_add(&wlr_output->events.mode, &output->mode);

	/* Adds this to the output layout. The add_auto function arranges outputs
	 * from left-to-right in the order they appear. A more sophisticated
	 * compositor would let the user configure the arrangement of outputs in the
	 * layout. */
	wlr_output_layout_add_auto(server->output_layout, wlr_output);

	/* Creating the global adds a wl_output global to the display, which Wayland
	 * clients can see to find out information about the output (such as
	 * DPI, scale factor, manufacturer, etc). */
	wlr_output_create_global(wlr_output);

	wlr_output_enable(wlr_output, true);
	wlr_output_commit(wlr_output);
}

/* Called when the surface is mapped, or ready to display on-screen. */
static void
xdg_surface_map(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_view *view = wl_container_of(listener, view, map);
	view->mapped             = true;
	focus_view(view, view->xdg_surface->surface);
}

/* Called when the surface is unmapped, and should no longer be shown. */
static void
xdg_surface_unmap(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_view *view = wl_container_of(listener, view, unmap);
	view->mapped             = false;
}

/* Called when the surface is destroyed and should never be shown again. */
static void
xdg_surface_destroy(struct wl_listener *listener, void *data)
{
	(void)data;
	struct inaban_view *view = wl_container_of(listener, view, destroy);
	wl_list_remove(&view->link);
	free(view);
}

static void
xdg_deny_request(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
}

/* raised when wlr_xdg_shell receives a new xdg surface from a client,
 * either a toplevel (application window) or popup. */
static void
server_new_xdg_surface(struct wl_listener *listener, void *data)
{
	struct inaban_server *server        = wl_container_of(listener, server, new_xdg_surface);
	struct wlr_xdg_surface *xdg_surface = data;
	if(xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) return;

	/* Allocate a inaban_view for this surface */
	struct inaban_view *view = calloc(1, sizeof(struct inaban_view));
	view->server             = server;
	view->xdg_surface        = xdg_surface;

	/* Listen to the various events it can emit */
	view->map.notify = xdg_surface_map;
	wl_signal_add(&xdg_surface->events.map, &view->map);
	view->unmap.notify = xdg_surface_unmap;
	wl_signal_add(&xdg_surface->events.unmap, &view->unmap);
	view->destroy.notify = xdg_surface_destroy;
	wl_signal_add(&xdg_surface->events.destroy, &view->destroy);

	/* cotd */
	struct wlr_xdg_toplevel *toplevel = xdg_surface->toplevel;
	view->request_move.notify         = xdg_deny_request;
	wl_signal_add(&toplevel->events.request_move, &view->request_move);
	view->request_resize.notify = xdg_deny_request;
	wl_signal_add(&toplevel->events.request_resize, &view->request_resize);

	/* Add it to the list of views. */
	wl_list_insert(&server->views, &view->link);
}

static void
handle_request_set_primary_selection(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}
	struct inaban_server *server = wl_container_of(listener, server, request_set_primary_selection);

	struct wlr_seat_request_set_primary_selection_event *event = data;

	wlr_seat_set_primary_selection(server->seat, event->source, event->serial);
}

static void
handle_request_set_selection(struct wl_listener *listener, void *data)
{
	if(server.locked == true)
	{
		return;
	}
	struct inaban_server *server = wl_container_of(listener, server, request_set_selection);

	struct wlr_seat_request_set_selection_event *event = data;

	wlr_seat_set_selection(server->seat, event->source, event->serial);
}

void
sigterm_handler(int signal)
{
	(void)signal;
	wl_display_terminate(server.wl_display);
}

void
usage(char *argv0)
{
	printf("Usage: %s [-s startup command]\n", argv0);
}

int
main(int argc, char *argv[])
{
	wlr_log_init(WLR_DEBUG, NULL);
	char *startup_cmdv[MAX_STARTUP_CMD] = {NULL};
	int startup_cmdc                    = 0;
	int ret                             = 0;

	struct wlr_server_decoration_manager *server_decoration_manager = NULL;
	struct wlr_xdg_decoration_manager_v1 *xdg_decoration_manager    = NULL;

	server.locked = false;

	if((getuid() * geteuid() * getgid() * getegid()) == 0)
	{
		wlr_log(WLR_ERROR, "running as root, refusing to continue");
		return 1;
	}

	// handle SIGTERM signals
	signal(SIGTERM, sigterm_handler);

	int c;
	while((c = getopt(argc, argv, "s:h")) != -1)
	{
		switch(c)
		{
		case 's':
			if(startup_cmdc < MAX_STARTUP_CMD)
			{
				startup_cmdv[startup_cmdc] = optarg;
				startup_cmdc++;
			}
			else
			{
				wlr_log(WLR_INFO,
				        "Ignoring ‘-s %s’, max number of startup commands (%d) reached.",
				        optarg,
				        MAX_STARTUP_CMD);
			}
			break;
		default: usage(argv[0]); return 0;
		}
	}
	if(optind < argc)
	{
		usage(argv[0]);
		return 0;
	}

	char *terminal = getenv("TERMINAL");
	if(NULL != terminal)
	{
		termcmd[0] = terminal;
	}

	server.wl_display = wl_display_create();
	if(!server.wl_display)
	{
		wlr_log(WLR_ERROR, "Cannot allocate a Wayland display");
		return 1;
	}

	server.backend = wlr_backend_autocreate(server.wl_display);
	if(!server.backend)
	{
		wlr_log(WLR_ERROR, "Unable to create the wlroots backend");
		ret = 1;
		goto end;
	}

	/* If we don't provide a renderer, autocreate makes a GLES2 renderer for us.
	 * The renderer is responsible for defining the various pixel formats it
	 * supports for shared memory, this configures that for clients. */
	server.renderer = wlr_renderer_autocreate(server.backend);
	if(!server.renderer)
	{
		wlr_log(WLR_ERROR, "Failed to create renderer");
		ret = 1;
		goto end;
	}

	server.allocator = wlr_allocator_autocreate(server.backend, server.renderer);

	wlr_renderer_init_wl_display(server.renderer, server.wl_display);

	/* This creates some hands-off wlroots interfaces. The compositor is
	 * necessary for clients to allocate surfaces and the data device manager
	 * handles the clipboard. Each of these wlroots interfaces has room for you
	 * to dig your fingers in and play with their behavior if you want. */
	if(!wlr_compositor_create(server.wl_display, server.renderer))
	{
		wlr_log(WLR_ERROR, "Unable to create the wlroots compositor");
		ret = 1;
		goto end;
	}

	if(!wlr_data_device_manager_create(server.wl_display))
	{
		wlr_log(WLR_ERROR, "Unable to create the data device manager");
		ret = 1;
		goto end;
	}

	/* Creates an output layout, which a wlroots utility for working with an
	 * arrangement of screens in a physical layout. */
	server.output_layout = wlr_output_layout_create();
	if(!server.output_layout)
	{
		wlr_log(WLR_ERROR, "Unable to create output layout");
		ret = 1;
		goto end;
	}

	/* Configure a listener to be notified when new outputs are available on the
	 * backend. */
	wl_list_init(&server.outputs);
	server.new_output.notify = server_new_output;
	assert(server.backend);
	wl_signal_add(&server.backend->events.new_output, &server.new_output);

	/* Set up our list of views and the xdg-shell. The xdg-shell is a Wayland
	 * protocol which is used for application windows. For more detail on
	 * shells, refer to my article:
	 *
	 * https://drewdevault.com/2018/07/29/Wayland-shells.html
	 */
	wl_list_init(&server.views);
	server.xdg_shell              = wlr_xdg_shell_create(server.wl_display);
	server.new_xdg_surface.notify = server_new_xdg_surface;
	wl_signal_add(&server.xdg_shell->events.new_surface, &server.new_xdg_surface);

	server_decoration_manager = wlr_server_decoration_manager_create(server.wl_display);
	if(!server_decoration_manager)
	{
		wlr_log(WLR_ERROR, "Unable to create the server decoration manager");
		ret = 1;
		goto end;
	}

	wlr_server_decoration_manager_set_default_mode(server_decoration_manager,
	                                               WLR_SERVER_DECORATION_MANAGER_MODE_SERVER);

	xdg_decoration_manager = wlr_xdg_decoration_manager_v1_create(server.wl_display);
	wl_signal_add(&xdg_decoration_manager->events.new_toplevel_decoration,
	              &server.xdg_toplevel_decoration);
	server.xdg_toplevel_decoration.notify = handle_xdg_toplevel_decoration;

	/*
	 * Creates a cursor, which is a wlroots utility for tracking the cursor
	 * image shown on screen.
	 */
	server.cursor = wlr_cursor_create();
	wlr_cursor_attach_output_layout(server.cursor, server.output_layout);

	/* Creates an xcursor manager, another wlroots utility which loads up
	 * Xcursor themes to source cursor images from and makes sure that cursor
	 * images are available at all scale factors on the screen (necessary for
	 * HiDPI support). We add a cursor theme at scale factor 1 to begin with. */
	server.cursor_mgr = wlr_xcursor_manager_create(NULL, 24);
	wlr_xcursor_manager_load(server.cursor_mgr, 1);

	/*
	 * wlr_cursor *only* displays an image on screen. It does not move around
	 * when the pointer moves. However, we can attach input devices to it, and
	 * it will generate aggregate events for all of them. In these events, we
	 * can choose how we want to process them, forwarding them to clients and
	 * moving the cursor around. More detail on this process is described in my
	 * input handling blog post:
	 *
	 * https://drewdevault.com/2018/07/17/Input-handling-in-wlroots.html
	 *
	 * And more comments are sprinkled throughout the notify functions above.
	 */
	server.cursor_motion.notify = server_cursor_motion;
	wl_signal_add(&server.cursor->events.motion, &server.cursor_motion);
	server.cursor_motion_absolute.notify = server_cursor_motion_absolute;
	wl_signal_add(&server.cursor->events.motion_absolute, &server.cursor_motion_absolute);
	server.cursor_button.notify = server_cursor_button;
	wl_signal_add(&server.cursor->events.button, &server.cursor_button);
	server.cursor_axis.notify = server_cursor_axis;
	wl_signal_add(&server.cursor->events.axis, &server.cursor_axis);
	server.cursor_frame.notify = server_cursor_frame;
	wl_signal_add(&server.cursor->events.frame, &server.cursor_frame);

	/*
	 * Configures a seat, which is a single "seat" at which a user sits and
	 * operates the computer. This conceptually includes up to one keyboard,
	 * pointer, touch, and drawing tablet device. We also rig up a listener to
	 * let us know when new input devices are available on the backend.
	 */
	wl_list_init(&server.keyboards);
	server.new_input.notify = server_new_input;
	wl_signal_add(&server.backend->events.new_input, &server.new_input);
	server.seat = wlr_seat_create(server.wl_display, "seat0");
	if(!server.seat)
	{
		wlr_log_errno(WLR_ERROR, "Unable to create wlroots seat");
		ret = 1;
		goto end;
	}
	server.request_cursor.notify = seat_request_cursor;
	wl_signal_add(&server.seat->events.request_set_cursor, &server.request_cursor);

	/* Clipboard protocols */
	wlr_data_control_manager_v1_create(server.wl_display);
	wlr_primary_selection_v1_device_manager_create(server.wl_display);
	/* Clipboard requests handlers */
	server.request_set_selection.notify = handle_request_set_selection;
	wl_signal_add(&server.seat->events.request_set_selection, &server.request_set_selection);
	server.request_set_primary_selection.notify = handle_request_set_primary_selection;
	wl_signal_add(&server.seat->events.request_set_primary_selection,
	              &server.request_set_primary_selection);

	/* Add a Unix socket to the Wayland display. */
	const char *socket = wl_display_add_socket_auto(server.wl_display);
	if(!socket)
	{
		wlr_log_errno(WLR_ERROR, "Unable to open Wayland socket");
		wlr_backend_destroy(server.backend);
		ret = 1;
		goto end;
	}

	/* Start the backend. This will enumerate outputs and inputs, become the DRM
	 * master, etc */
	if(!wlr_backend_start(server.backend))
	{
		wlr_log(WLR_ERROR, "Unable to start the wlroots backend");
		wlr_backend_destroy(server.backend);
		wl_display_destroy(server.wl_display);
		ret = 1;
		goto end;
	}

	/* Set the WAYLAND_DISPLAY environment variable to our socket and run the
	 * startup command if requested. */
	unsetenv("DISPLAY");
	setenv("WAYLAND_DISPLAY", socket, true);
	for(int i = 0; i < startup_cmdc; i++)
	{
		if(fork() == 0)
		{
			execl("/bin/sh", "/bin/sh", "-c", startup_cmdv[i], (void *)NULL);
		}
	}
	/* Run the Wayland event loop. This does not return until you exit the
	 * compositor. Starting the backend rigged up all of the necessary event
	 * loop configuration to listen to libinput events, DRM events, generate
	 * frame events at the refresh rate, and so on. */
	wlr_log(WLR_INFO, "Running Wayland compositor on WAYLAND_DISPLAY=%s", socket);
	wl_display_run(server.wl_display);

end:
	if(server.seat) wlr_seat_destroy(server.seat);
	wl_display_destroy_clients(server.wl_display);
	wl_display_destroy(server.wl_display);
	return ret;
}
