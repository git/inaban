// SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CONFIG_H
#define CONFIG_H

//static const char *menucmd[] = {"bemenu-run", NULL};
static char *termcmd[] = {"svte", NULL};

static const float background_color[4] = {0.11f, 0.11f, 0.11f, 1.0f}; // approx. gruvbox hard-dark
static const float border_color[4]     = {0.25f, 0.25f, 0.50f, 1.0f};
static const float locked_color[4]     = {0.50f, 0.25f, 0.25f, 1.0f};

#define BORDER_SIZE 1

// See `enum wlr_keyboard_modifier` in `<wlr/types/wlr_keyboard.h>`
//#define ModMask WLR_MODIFIER_ALT
#define ModMask WLR_MODIFIER_LOGO
#define ShiftMask WLR_MODIFIER_SHIFT
// See <xkbcommon/xkbcommon-keysyms.h>
#define ModKey XKB_KEY_Super_L

// clang-format off
static Shortcut shortcuts[] = {
    /* modifier,            keysym,         function,       argument */
//    {ModMask,                XKB_KEY_p,      spawn,          {.v = menucmd}},
    {ModMask,                XKB_KEY_Return, spawn,          {.v = termcmd}},
    {ModMask & ShiftMask,    XKB_KEY_q,      quit,           {0}},
    {ModMask,                XKB_KEY_l,      lock,           {0}},
//    {MODKEY | ShiftMask,    XKB_KEY_c,      killclient,     {0}},
//    {MODKEY,                XKB_KEY_j,      focusstack,     {.i = +1}},
//    {MODKEY,                XKB_KEY_k,      focusstack,     {.i = -1}},
};
// clang-format on
#endif /* CONFIG_H */
