# SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
# SPDX-License-Identifier: BSD-3-Clause

# POSIX-ish Makefile with extensions common to *BSD and GNU such as:
# - Usage of backticks for shell evaluation
# - Usage of ?= for defining variables when not already defined
# - Usage of += for appending to a variable

VERSION = 0.0.1
VERSION_FULL = $(VERSION)`./version.sh`

PREFIX  = /usr/local
BINDIR  = $(PREFIX)/bin
MANDIR  = $(PREFIX)/share/man
DATADIR = $(PREFIX)/share/inaban

CC        ?= cc
CFLAGS    ?= -g -Wall -Wextra -Wconversion -Wsign-conversion -O2
DBG       ?=
PKGCONFIG ?= pkg-config

DEPS  = xkbcommon wlroots wayland-server
EXE   = inaban
TRANS =
SRC   = xdg-decoration.c commands.c inaban.c

CDEPS  = -DDATADIR=\"$(DATADIR)\" -DPACKAGE=\"$(PACKAGE)\" -D_POSIX_C_SOURCE=200809L -I./protocols
CDEPS += `pkg-config --cflags $(DEPS)`
LIBS   = `pkg-config --libs $(DEPS)`

WAYLAND_PROTOCOLS=`pkg-config --variable=pkgdatadir wayland-protocols`
WAYLAND_SCANNER=`pkg-config --variable=wayland_scanner wayland-scanner`

all: protocols/xdg-shell-protocol.c $(EXE) $(TRANS)

inaban: $(SRC) inaban.h config.h
	$(CC) -std=c99 $(CFLAGS) $(CDEPS) -o $@ $(SRC) $(LDFLAGS) $(LIBS)

protocols/xdg-shell-protocol.h:
	$(WAYLAND_SCANNER) server-header $(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

protocols/xdg-shell-protocol.c: protocols/xdg-shell-protocol.h
	$(WAYLAND_SCANNER) private-code $(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -p $(EXE) $(DESTDIR)$(BINDIR)/$(EXE)
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -p $(EXE).1 $(DESTDIR)$(MANDIR)/man1
	#mkdir -p $(DESTDIR)$(DATADIR)/locale
	#cp -r locale/ $(DESTDIR)$(DATADIR)

clean:
	rm -fr locale $(EXE) protocols/xdg-shell-protocol.c protocols/xdg-shell-protocol.h

format: *.c *.h
	clang-format -style=file -assume-filename=.clang-format -i *.c *.h

.PHONY: clean install
