// SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
// SPDX-License-Identifier: BSD-3-Clause

#include "inaban.h"

#include <stdlib.h>
#include <unistd.h> /* fork(), execvp() */

// defined in inaban.c
extern struct inaban_server server;

void
spawn(const Arg *arg)
{
	if(fork() == 0)
	{
		setsid();
		execvp(((char **)arg->v)[0], (char **)arg->v);
		fprintf(stderr, "execvp %s", ((char **)arg->v)[0]);
		perror(" failed");
		exit(EXIT_SUCCESS);
	}
}

/* Note: this function only deals with keyboard focus. */
void
focus_view(struct inaban_view *view, struct wlr_surface *surface)
{
	if(view == NULL) return;
	struct inaban_server *server     = view->server;
	struct wlr_seat *seat            = server->seat;
	struct wlr_surface *prev_surface = seat->keyboard_state.focused_surface;
	if(prev_surface == surface) return; /* Don't re-focus an already focused surface. */
	if(prev_surface)
	{
		/*
		 * Deactivate the previously focused surface. This lets the client know
		 * it no longer has focus and the client will repaint accordingly, e.g.
		 * stop displaying a caret.
		 */
		struct wlr_xdg_surface *previous =
		    wlr_xdg_surface_from_wlr_surface(seat->keyboard_state.focused_surface);
		wlr_xdg_toplevel_set_activated(previous, false);
	}
	struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(seat);
	/* Move the view to the front */
	wl_list_remove(&view->link);
	wl_list_insert(&server->views, &view->link);
	/* Activate the new surface */
	wlr_xdg_toplevel_set_activated(view->xdg_surface, true);
	/*
	 * Tell the seat to have the keyboard enter this surface. wlroots will keep
	 * track of this and automatically send key events to the appropriate
	 * clients without additional work on your part.
	 */
	wlr_seat_keyboard_notify_enter(seat,
	                               view->xdg_surface->surface,
	                               keyboard->keycodes,
	                               keyboard->num_keycodes,
	                               &keyboard->modifiers);
}

void
quit(const Arg *arg)
{
	(void)arg;
	wl_display_terminate(server.wl_display);
}

void
lock(const Arg *arg)
{
	(void)arg;
	server.locked = true;
}
