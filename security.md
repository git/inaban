<!--
SPDX-FileCopyrightText: 2019-2022 inaban Authors <https://hacktivis.me/git/inaban>
SPDX-License-Identifier: BSD-3-Clause
-->

# Security
Until version 1.0 this serves as a roadmap.

Report security issues via an email to <contact+inaban@hacktivis.me> with <https://hacktivis.me/reop.pub> as my reop public key.

## Design
### Focus
- Keyboard & Pointer focus is synchronised (keyboard focus warps the pointer)
- Applications cannot steal focus unless explicitely launched by the user

### Special Permissions
Special permissions are needed for: Screen capturing/recording, app-requested fullscreen, snooping (including for accessibility purposes), monitor settings.

- Because of linux's design, unless an application is registered and launched by the compositor a consent pop-in dialog is displayed

### Fullscreen
- Applications can't fullscreen themselves (until proper process authentication on linux is discovered)

### Lockscreen
- Inspired by <https://www.jwz.org/xscreensaver/toolkits.html>
- Wayland clients compositing (input & graphics) is disabled while locked; this also means that screen recording gets refused or when already authorized gets blank output.
- Normal applications get title/`app_id`/… displayed when Logo is pressed
- Separated authentication process launched by the compositor
	- setuid-root is frowned upon, use TCB shadow
	- PAM is unsupported
	- Patches for OpenBSD Authentication are welcome
	- On success it returned a unique hash to stdout and returned 0
	- stderr for Error messages
- Screensavers are unsupported for now, they could be handed a wayland file descriptor to connect to
